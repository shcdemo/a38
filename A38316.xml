<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A38316">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>The Lords and Commons in Parliament assembled do order and appoint this day fortnight for a day of thanksgiving for taking in of Dartmouth to be kept within the cities of London and Westminster ... and this day three weeks for all other places in the countrey.</title>
    <author>England and Wales. Parliament.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A38316 of text R30443 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing E2815). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A38316</idno>
    <idno type="STC">Wing E2815</idno>
    <idno type="STC">ESTC R30443</idno>
    <idno type="EEBO-CITATION">11322205</idno>
    <idno type="OCLC">ocm 11322205</idno>
    <idno type="VID">47447</idno>
    <idno type="PROQUESTGOID">2240860527</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A38316)</note>
    <note>Transcribed from: (Early English Books Online ; image set 47447)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1460:23)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>The Lords and Commons in Parliament assembled do order and appoint this day fortnight for a day of thanksgiving for taking in of Dartmouth to be kept within the cities of London and Westminster ... and this day three weeks for all other places in the countrey.</title>
      <author>England and Wales. Parliament.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>s.n.,</publisher>
      <pubPlace>[London :</pubPlace>
      <date>1646]</date>
     </publicationStmt>
     <notesStmt>
      <note>At head of text: Die Iovis 22. Ianuarii 1645.</note>
      <note>Imprint supplied by Wing.</note>
      <note>"Ordered by the Commons assembled in Parliament, that this order be forthwith printed, published and dispersed into the severall counties of the Kingdome. Hen. Elsynge, Cler. Parl. Dom. Com."</note>
      <note>Reproduction of original in the Harvard Law School Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- History -- Civil War, 1642-1649.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A38316</ep:tcp>
    <ep:estc> R30443</ep:estc>
    <ep:stc> (Wing E2815). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>Die Iovis 22 Ianuarii 1645. The Lords and Commons in Parliament assembled do order and appoint this day fortnight for a day of thanksgiving,</ep:title>
    <ep:author>England and Wales. Parliament</ep:author>
    <ep:publicationYear>1646</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>97</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-09</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-11</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2009-01</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2009-01</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A38316-e10">
  <body xml:id="A38316-e20">
   <pb facs="tcp:47447:1" rend="simple:additions" xml:id="A38316-001-a"/>
   <div type="Parliamentary_order" xml:id="A38316-e30">
    <opener xml:id="A38316-e40">
     <dateline xml:id="A38316-e50">
      <date xml:id="A38316-e60">
       <w lemma="die" pos="fla" xml:id="A38316-001-a-0010">Die</w>
       <w lemma="iovis" pos="fla" xml:id="A38316-001-a-0020">Iovis</w>
       <w lemma="22" pos="crd" xml:id="A38316-001-a-0030">22</w>
       <w lemma="ianuarii" pos="fla" xml:id="A38316-001-a-0040">Ianuarii</w>
       <w lemma="1645" pos="crd" xml:id="A38316-001-a-0050">1645</w>
      </date>
     </dateline>
    </opener>
    <p xml:id="A38316-e70">
     <w lemma="the" pos="d" rend="decorinit" xml:id="A38316-001-a-0060">THE</w>
     <w lemma="lord" pos="n2" xml:id="A38316-001-a-0070">LORDS</w>
     <w lemma="and" pos="cc" xml:id="A38316-001-a-0080">and</w>
     <w lemma="commons" pos="n2" xml:id="A38316-001-a-0090">Commons</w>
     <w lemma="in" pos="acp" xml:id="A38316-001-a-0100">in</w>
     <w lemma="parliament" pos="n1" xml:id="A38316-001-a-0110">Parliament</w>
     <w lemma="assemble" pos="vvn" xml:id="A38316-001-a-0120">assembled</w>
     <w lemma="do" pos="vvb" xml:id="A38316-001-a-0130">do</w>
     <w lemma="order" pos="n1" xml:id="A38316-001-a-0140">Order</w>
     <w lemma="and" pos="cc" xml:id="A38316-001-a-0150">and</w>
     <w lemma="appoint" pos="vvi" xml:id="A38316-001-a-0160">appoint</w>
     <w lemma="this" pos="d" xml:id="A38316-001-a-0170">this</w>
     <w lemma="day" pos="n1" xml:id="A38316-001-a-0180">day</w>
     <w lemma="fortnight" pos="n1" reg="fortnight" xml:id="A38316-001-a-0190">fort-night</w>
     <w lemma="for" pos="acp" xml:id="A38316-001-a-0200">for</w>
     <w lemma="a" pos="d" xml:id="A38316-001-a-0210">a</w>
     <w lemma="day" pos="n1" xml:id="A38316-001-a-0220">day</w>
     <w lemma="of" pos="acp" xml:id="A38316-001-a-0230">of</w>
     <w lemma="thanksgiving" pos="n1" xml:id="A38316-001-a-0240">Thanksgiving</w>
     <pc xml:id="A38316-001-a-0250">,</pc>
     <w lemma="for" pos="acp" xml:id="A38316-001-a-0260">for</w>
     <w lemma="take" pos="vvg" xml:id="A38316-001-a-0270">taking</w>
     <w lemma="in" pos="acp" xml:id="A38316-001-a-0280">in</w>
     <w lemma="of" pos="acp" xml:id="A38316-001-a-0290">of</w>
     <hi xml:id="A38316-e80">
      <w lemma="Dartmouth" pos="nn1" xml:id="A38316-001-a-0300">DARTMOUTH</w>
      <pc xml:id="A38316-001-a-0310">,</pc>
     </hi>
     <w lemma="to" pos="prt" xml:id="A38316-001-a-0320">to</w>
     <w lemma="be" pos="vvi" xml:id="A38316-001-a-0330">be</w>
     <w lemma="keep" pos="vvn" xml:id="A38316-001-a-0340">kept</w>
     <w lemma="within" pos="acp" xml:id="A38316-001-a-0350">within</w>
     <w lemma="the" pos="d" xml:id="A38316-001-a-0360">the</w>
     <w lemma="city" pos="n2" xml:id="A38316-001-a-0370">Cities</w>
     <w lemma="of" pos="acp" xml:id="A38316-001-a-0380">of</w>
     <hi xml:id="A38316-e90">
      <w lemma="London" pos="nn1" xml:id="A38316-001-a-0390">London</w>
      <w lemma="and" pos="cc" xml:id="A38316-001-a-0400">and</w>
      <w lemma="Westminster" pos="nn1" xml:id="A38316-001-a-0410">Westminster</w>
      <pc xml:id="A38316-001-a-0420">,</pc>
     </hi>
     <w lemma="line" pos="n2" xml:id="A38316-001-a-0430">Lines</w>
     <w lemma="of" pos="acp" xml:id="A38316-001-a-0440">of</w>
     <w lemma="communication" pos="n1" xml:id="A38316-001-a-0450">Communication</w>
     <pc xml:id="A38316-001-a-0460">,</pc>
     <w lemma="and" pos="cc" xml:id="A38316-001-a-0470">and</w>
     <w lemma="ten" pos="crd" xml:id="A38316-001-a-0480">ten</w>
     <w lemma="mile" pos="n2" xml:id="A38316-001-a-0490">miles</w>
     <w lemma="about" pos="acp" xml:id="A38316-001-a-0500">about</w>
     <pc xml:id="A38316-001-a-0510">;</pc>
     <w lemma="and" pos="cc" xml:id="A38316-001-a-0520">And</w>
     <w lemma="this" pos="d" xml:id="A38316-001-a-0530">this</w>
     <w lemma="day" pos="n1" xml:id="A38316-001-a-0540">day</w>
     <w lemma="three" pos="crd" xml:id="A38316-001-a-0550">three</w>
     <w lemma="week" pos="n2" xml:id="A38316-001-a-0560">weeks</w>
     <w lemma="for" pos="acp" xml:id="A38316-001-a-0570">for</w>
     <w lemma="all" pos="d" xml:id="A38316-001-a-0580">all</w>
     <w lemma="other" pos="d" xml:id="A38316-001-a-0590">other</w>
     <w lemma="place" pos="n2" xml:id="A38316-001-a-0600">places</w>
     <w lemma="in" pos="acp" xml:id="A38316-001-a-0610">in</w>
     <w lemma="the" pos="d" xml:id="A38316-001-a-0620">the</w>
     <w lemma="country" pos="n1" reg="country" xml:id="A38316-001-a-0630">Countrey</w>
     <pc unit="sentence" xml:id="A38316-001-a-0640">.</pc>
    </p>
   </div>
   <div type="license" xml:id="A38316-e100">
    <p xml:id="A38316-e110">
     <w lemma="order" pos="j-vn" xml:id="A38316-001-a-0650">ORdered</w>
     <w lemma="by" pos="acp" xml:id="A38316-001-a-0660">by</w>
     <w lemma="the" pos="d" xml:id="A38316-001-a-0670">the</w>
     <w lemma="commons" pos="n2" xml:id="A38316-001-a-0680">Commons</w>
     <w lemma="assemble" pos="vvn" xml:id="A38316-001-a-0690">assembled</w>
     <w lemma="in" pos="acp" xml:id="A38316-001-a-0700">in</w>
     <w lemma="parliament" pos="n1" xml:id="A38316-001-a-0710">Parliament</w>
     <pc xml:id="A38316-001-a-0720">,</pc>
     <w lemma="that" pos="cs" xml:id="A38316-001-a-0730">That</w>
     <w lemma="this" pos="d" xml:id="A38316-001-a-0740">this</w>
     <w lemma="order" pos="n1" xml:id="A38316-001-a-0750">Order</w>
     <w lemma="be" pos="vvb" xml:id="A38316-001-a-0760">be</w>
     <w lemma="forthwith" pos="av" xml:id="A38316-001-a-0770">forthwith</w>
     <w lemma="print" pos="vvn" xml:id="A38316-001-a-0780">printed</w>
     <w lemma="publish" pos="vvn" xml:id="A38316-001-a-0790">published</w>
     <w lemma="and" pos="cc" xml:id="A38316-001-a-0800">and</w>
     <w lemma="disperse" pos="vvn" xml:id="A38316-001-a-0810">dispersed</w>
     <w lemma="into" pos="acp" xml:id="A38316-001-a-0820">into</w>
     <w lemma="the" pos="d" xml:id="A38316-001-a-0830">the</w>
     <w lemma="several" pos="j" reg="several" xml:id="A38316-001-a-0840">severall</w>
     <w lemma="county" pos="n2" xml:id="A38316-001-a-0850">Counties</w>
     <w lemma="of" pos="acp" xml:id="A38316-001-a-0860">of</w>
     <w lemma="the" pos="d" xml:id="A38316-001-a-0870">the</w>
     <w lemma="kingdom" pos="n1" reg="kingdom" xml:id="A38316-001-a-0880">Kingdome</w>
     <pc unit="sentence" xml:id="A38316-001-a-0890">.</pc>
    </p>
    <closer xml:id="A38316-e120">
     <signed xml:id="A38316-e130">
      <w lemma="Hen." pos="ab" xml:id="A38316-001-a-0900">Hen.</w>
      <w lemma="Elsing" pos="nn1" reg="Elsing" xml:id="A38316-001-a-0920">Elsynge</w>
      <pc xml:id="A38316-001-a-0930">,</pc>
      <w lemma="cler." pos="ab" xml:id="A38316-001-a-0940">Cler.</w>
      <w lemma="parl." pos="ab" xml:id="A38316-001-a-0950">Parl.</w>
      <w lemma="dom." pos="ab" xml:id="A38316-001-a-0960">Dom.</w>
      <w lemma="com." pos="ab" xml:id="A38316-001-a-0970">Com.</w>
      <pc unit="sentence" xml:id="A38316-001-a-0980"/>
     </signed>
    </closer>
   </div>
  </body>
 </text>
</TEI>
